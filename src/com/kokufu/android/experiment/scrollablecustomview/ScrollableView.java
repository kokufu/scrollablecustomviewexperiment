package com.kokufu.android.experiment.scrollablecustomview;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class ScrollableView extends View {
    private static final int BOX_SIZE = 100;
    private static final int REAL_WIDTH = 4096;
    private static final int REAL_HEIGHT = 4096;

    public ScrollableView(Context context) {
        super(context);
    }
    public ScrollableView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public ScrollableView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected int computeHorizontalScrollRange() {
        return REAL_WIDTH;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected int computeVerticalScrollRange() {
        return REAL_HEIGHT;

    }


    // These methods are the same as super's

//    @Override
//    protected int computeHorizontalScrollOffset() {
//        return getScrollX();
//    }

//    @Override
//    protected int computeHorizontalScrollExtent() {
//        return getWidth();
//    }

//    @Override
//    protected int computeVerticalScrollOffset() {
//        return getScrollY();
//    }

//    @Override
//    protected int computeVerticalScrollExtent() {
//        return getHeight();
//    }


    float mPreX = 0;
    float mPreY = 0;
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
        case MotionEvent.ACTION_DOWN: {

            mPreX = event.getX();
            mPreY = event.getY();

            awakenScrollBars();
            break;
        }
        case MotionEvent.ACTION_MOVE: {
            float x = event.getX();
            float y = event.getY();
            int scrollX = getScrollX();
            int scrollY = getScrollY();
            scrollX += mPreX - x;
            scrollY += mPreY - y;

            // Clipping
            scrollX = (scrollX < 0) ? 0 :
                      (scrollX > REAL_WIDTH - getWidth()) ? REAL_WIDTH - getWidth() : scrollX;
            scrollY = (scrollY < 0) ? 0 :
                      (scrollY > REAL_HEIGHT - getHeight()) ? REAL_HEIGHT - getHeight() : scrollY;

            scrollTo(scrollX, scrollY);

            mPreX = x;
            mPreY = y;

            // Display Scroll bars
            awakenScrollBars();
            break;
        }
        default:
            break;
        }
        return true;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawColor(Color.BLACK);
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        int leftTopBoxY = ((int) (getScrollY() / BOX_SIZE)) * BOX_SIZE;
        int leftTopBoxX = ((int) (getScrollX() / BOX_SIZE)) * BOX_SIZE;
        for (int y = leftTopBoxY; y < getScrollY() + getHeight(); y += BOX_SIZE) {
            for (int x = leftTopBoxX; x < getScrollX() + getWidth(); x += BOX_SIZE) {
                canvas.drawRect(x, y, x + BOX_SIZE - 1, y + BOX_SIZE - 1, paint);
            }
        }

        super.draw(canvas);
    }
}
