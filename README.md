ScrollableCustomViewExperiment
===============
Android の CustomView を作成した際、その中に Scroll Bar を表示する実験。
ブログ記事は以下。

[Android の Custom View で Scrollbar を使用する](http://kokufu.blogspot.jp/2012/07/android-custom-view-scrollbar.html)

## Eclispse へのインポート方法
New -> Project -> Android Project from Existing Code

